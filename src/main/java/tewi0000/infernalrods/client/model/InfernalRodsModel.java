package tewi0000.infernalrods.client.model;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.MathHelper;

public class InfernalRodsModel extends EntityModel<LivingEntity> {
    private final ModelRenderer[] rods;
    float a, b, c;

    public InfernalRodsModel(float size, float a, float b, float c) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.rods = new ModelRenderer[12];

        for(int i = 0; i < this.rods.length; ++i) {
            this.rods[i] = new ModelRenderer(this, 0, 16);
            this.rods[i].addBox(0.0F, 0.0F, 0.0F, 2.0F, 8.0F, 2.0F);
        }
    }

    public void setRotationAngles(LivingEntity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
        float f = ageInTicks * (float)Math.PI * -0.1F;

        for(int i = 0; i < 4; ++i) {
            this.rods[i].rotationPointY = -2.0F + MathHelper.cos(((float)(i * 2) + ageInTicks) * 0.25F);
            this.rods[i].rotationPointX = MathHelper.cos(f) * c;
            this.rods[i].rotationPointZ = MathHelper.sin(f) * c;
            ++f;
        }

        f = ((float)Math.PI / 4F) + ageInTicks * (float)Math.PI * 0.03F;

        for(int j = 4; j < 8; ++j) {
            this.rods[j].rotationPointY = 2.0F + MathHelper.cos(((float)(j * 2) + ageInTicks) * 0.25F);
            this.rods[j].rotationPointX = MathHelper.cos(f) * b;
            this.rods[j].rotationPointZ = MathHelper.sin(f) * b;
            ++f;
        }

        f = 0.47123894F + ageInTicks * (float)Math.PI * -0.05F;

        for(int k = 8; k < 12; ++k) {
            this.rods[k].rotationPointY = 11.0F + MathHelper.cos(((float)k * 1.5F + ageInTicks) * 0.5F);
            this.rods[k].rotationPointX = MathHelper.cos(f) * a;
            this.rods[k].rotationPointZ = MathHelper.sin(f) * a;
            ++f;
        }
    }

    @Override
    public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) {
        for (ModelRenderer rod : rods) {
            rod.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, red, green, blue, alpha);
        }
    }
}
