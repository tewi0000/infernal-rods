package tewi0000.infernalrods;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Rarity;
import tewi0000.infernalrods.shared.item.InfernalRodsItem;

public class ModItems {
    public static final Item INFERNAL_RODS = new InfernalRodsItem(new Item.Properties().group(ItemGroup.TRANSPORTATION).rarity(Rarity.RARE)).setRegistryName("infernal_rods");
}